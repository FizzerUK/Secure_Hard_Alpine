FROM alpine:3.18.2 as build

RUN apk update && \
    apk --no-cache upgrade && \
    apk --no-cache add \
       gcc \
       linux-headers \
       make \
       musl-dev \
       perl  && \
    rm -rf /var/cache/apk/*

COPY openssl.tar.gz /

WORKDIR /usr/local/src/openssl

RUN tar -zxf /openssl.tar.gz -C /usr/local/src/openssl --strip-components=1 && \
    perl ./Configure \
       --prefix=/usr \
       --libdir=lib \
       --openssldir=/etc/ssl \
       enable-ktls \
       shared \
       no-zlib \
       no-async \
       no-comp \
       no-idea \
       no-mdc2 \
       no-rc5 \
       no-ec2m \
       no-sm2 \
       no-sm4 \
       no-ssl3 \
       no-seed \
       no-weak-ssl-ciphers \
       enable-fips \
       enable-ec_nistp_64_gcc_128 \
       -Wa,--noexecstack && \
    make && \
    make install && \
    make install_fips && \
    openssl fipsinstall -out /etc/ssl/fipsmodule.cnf -module /usr/lib/ossl-modules/fips.so

FROM alpine:3.18.2

RUN apk update && \
    apk --no-cache upgrade && \
    apk --no-cache add ca-certificates && \
    rm -rf /var/cache/apk/*

COPY --from=build /usr/lib/ossl-modules/fips.so /usr/lib/ossl-modules/fips.so
COPY --from=build /etc/ssl/fipsmodule.cnf /etc/ssl/fipsmodule.cnf
COPY etc/issue /etc/issue
COPY certs/ /usr/local/share/ca-certificates

RUN update-ca-certificates && \
    sed -i 's/^default\s=\sdefault_sect/# default = default_sect/' /etc/ssl/openssl.cnf && \
    sed -i 's/^#\s\.include\sfipsmodule.cnf/\.include \/etc\/ssl\/fipsmodule.cnf/' /etc/ssl/openssl.cnf && \
    sed -i 's/^#\sfips\s=\sfips_sect/fips = fips_sect\nbase = base_sect\n\n[base_sect]\nactivate=1/' /etc/ssl/openssl.cnf && \
    sed -i "s/umask.*/umask 077/g" /etc/profile && \
    sed -i -r '/^(root|nobody)/!d' /etc/group /etc/passwd /etc/shadow && \
    chown root:root /etc/shadow && \
    chmod 000 /etc/shadow
